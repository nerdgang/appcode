//
//  NKControls.swift
//  NerdKit
//
//  Created by Havic on 11/3/15.
//  Copyright © 2015 Nerd Gang Inc. All rights reserved.
//
/*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit

@objc public protocol NGControllersDelegate{
  @objc optional func reloadTableInfo()
  @objc optional func handleRefresh(_ refreshControl:UIRefreshControl)
  @objc optional func parseModelObjects()
}

open class NKControls: NSObject,NGControllersDelegate {
  
public var ngControls: NGControllersDelegate?
  public let refreshControllerView = UIView()
  public var jsonArray = NSArray()
  public var jsonDictionary = NSDictionary()
  
  
  /**
   Json Loader Method the downloading of json and the values being passed in from custom vars
   
   - parameter dataTask: -> shared session object dataTaskWithUrl(NSURL!,{ (NSData, NSResponse, NSError)} )
   
   - returns: The Array which is created from the info temporaily stored it in tempArr
   */
  
  open func loadThatJson(_ jsonUrl:URL, isArray:Bool){
    
//    guard currentReachabilityStatus != .notReachable else {
//        print("We have no net homie")
//        
//        return
//    }
//    
//    let session = URLSession.shared.dataTask(with: jsonUrl) { (data, response, error) -> Void in
//      if (isArray){
//        self.jsonArray = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)) as! NSArray
//      }
//      else{
//        self.jsonDictionary = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)) as! NSDictionary
//      }
//      
//      DispatchQueue.main.async(execute: { () -> Void in
//        self.ngControls?.reloadTableInfo!()
//      })
//    }
//    
//    
//    session.resume()
    
    jsonParsing(jsonUrl, isArray: isArray)
  }
    
    fileprivate func jsonParsing(_ jsonUrl:URL, isArray:Bool){
        guard currentReachabilityStatus != .notReachable else {
            print("We have no net homie")
            
            return
        }
        
        let session = URLSession.shared.dataTask(with: jsonUrl) { (data, response, error) -> Void in
            if (isArray){
                self.jsonArray = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)) as! NSArray
            }
            else{
                self.jsonDictionary = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)) as! NSDictionary
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                self.ngControls?.reloadTableInfo!()
            })
        }
        
        
        session.resume()
    }
  
  /**
   Json Loader Method the downloading of json and the values being passed in from the json Object
   
   - parameter dataTask: -> shared session object dataTaskWithUrl(NSURL!,{ (NSData, NSResponse, NSError)} )
   
   - returns: The blogArray which is created from the info we temporaily stored it in tempArr
   
   IMPORTANT NOTE: When calling this method you need to also implement the "parseModelObjects" method, to parse any model objects you would like to use.
   */
  
  open func loadThatJsonWithModelObjects(_ jsonUrl:URL, isArray:Bool){
    let session = URLSession.shared.dataTask(with: jsonUrl) { (data, response, error) -> Void in
      if(isArray){
        self.jsonArray = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)) as! NSArray
      }else{
        self.jsonDictionary = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)) as! NSDictionary
      }
      self.ngControls?.parseModelObjects!()
      
      DispatchQueue.main.async(execute: { () -> Void in
        self.ngControls?.reloadTableInfo!()
      })
    }
    session.resume()
  }
  
  
  /**
   
   UIRefreshControl Method:
   
   1 - creates a instance of UIRefreshControl
   
   2- adds a target to the instance
   
   3 - Creates a bg color for refreshcontrol(UIColor param)
   
   4 - Creates color for refreshcontrol(UIColor param)
   
   - returns: UIRefreshControl object
   
   IMPORTANT NOTE: When calling this method you need to also implement the "handleRefresh" method that this method is looking for.
   Also you need to add "refreshControllerView" to your own view for it to be visiable
   
   */
  
  open class func refreshController(_ spinnerBgColor:UIColor, spinnerTintColor:UIColor)->UIRefreshControl{
    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(NGControllersDelegate.handleRefresh(_:)), for: UIControlEvents.valueChanged)
    refreshControl.backgroundColor = spinnerBgColor
    refreshControl.tintColor = spinnerTintColor
    
    return refreshControl
  }
}
