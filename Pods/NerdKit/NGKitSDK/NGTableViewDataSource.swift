//
//  NGTableViewDataSource.swift
//  NerdKit
//
//  Created by Havic on 12/6/16.
//  Copyright © 2016 Nerd Gang Inc. All rights reserved.
//

import Foundation
import UIKit

public class NGTableDataSource: NSObject, UITableViewDataSource {
    
    var items: [AnyObject]
    var cellIdentifier: String
    
    init(items: [AnyObject]!, cellIdentifier: String!) {
        self.items = items
        self.cellIdentifier = cellIdentifier
        
        super.init()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = items[indexPath.row] as? String
        
        return cell
    }
    
}
