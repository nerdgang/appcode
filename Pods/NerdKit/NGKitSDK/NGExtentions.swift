//
//  NGExtentions.swift
//  NerdKit
//
//  Created by Havic on 8/8/15.
//  Copyright (c) 2015 Nerd Gang Inc. All rights reserved.
//
/*
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Foundation
import UIKit
import AVFoundation
import SystemConfiguration

extension String{
    /**
     
     Strips extra HTML characters from String and replaces with regular characters.
     
     - returns: String
     
     */
    
    mutating func changeHtmlToString() -> String{
        
        var newString = self
        newString = newString.replacingOccurrences(of: "&#038;", with: "&")
        newString = newString.replacingOccurrences(of: "&#8211;", with: "-")
        newString = newString.replacingOccurrences(of: "&#8217;", with: "'")
        newString = newString.replacingOccurrences(of: "&#8220;", with: "\" ")
        newString = newString.replacingOccurrences(of: "&#8221;", with: "\" ")
        
        return newString
    }
    
    var length: Int{return self.characters.count}
    
}

public extension UIColor{
    
    public static func twlBlue()->UIColor{return UIColor(red: 56/255, green: 212/255, blue: 198/255, alpha: 1.0)}
    
    public func MyGradients() ->CAGradientLayer {
        let gradient:CAGradientLayer = CAGradientLayer()
        let color1 = UIColor(red: 30/255, green: 144/255, blue: 255/255, alpha: 1)
        let color2 = UIColor(red: 2/255, green: 84/255, blue: 164/255, alpha: 1)
        gradient.colors = Array(arrayLiteral: color1.cgColor,color2.cgColor)
        
        return gradient
    }
    
    public static func babyBlue()->UIColor{
        return UIColor(red: 9/255, green: 147/255, blue: 251/255, alpha: 1)
    }
}

extension UIImage{
    convenience init?(createFromThis:String?) {
        guard let urlString = createFromThis, let stringUrl = URL(string: urlString), let dataObject = try? Data(contentsOf: stringUrl) else{self.init()
            return}
        
        
        self.init(data: dataObject)
        
    }
    
}

public extension NSObject{
    
    
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
    
}
