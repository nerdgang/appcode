//
//  HomeViewController.swift
//  appCode
//
//  Created by Havic on 2/23/17.
//  Copyright © 2017 Nerd Gang LLC. All rights reserved.
//

import UIKit
import PureLayout

//TODO: Create a singleton to

class HomeViewController: UIViewController,TWLCollectionViewDelegate {
    typealias deviceScreen = UIScreen
    
    var settingsBTN: UIButton = {
        let settingsBTN = UIButton()
        settingsBTN.setImage(UIImage(named: String(describing: AssestImage.setting)), for: .normal)
        return settingsBTN
    }()
    
    var searchBTN: UIButton =  {
        let searchBTN = UIButton()
        searchBTN.setImage(UIImage(named: String(describing: AssestImage.search)), for: .normal)
        return searchBTN
    }()
    
    var caroselView: UIView = {
        let caroselView = UIView()
        
        caroselView.frame = CGRect(x: 0, y: 0, width: deviceScreen.main.bounds.width, height: 150)
        return caroselView
    }()
    
    var homeScrollVIew: UIScrollView = {
        let homeScroll = UIScrollView()
        homeScroll.frame = CGRect(x: 0, y: 200, width: deviceScreen.main.bounds.width, height: deviceScreen.main.bounds.height)
        homeScroll.isScrollEnabled = true
        //homeScroll.backgroundColor = .green
        return homeScroll
    }()
    
    var whatsHot: TWLCollectionViewController = {
        let whatIsHot = TWLCollectionViewController()
        return whatIsHot
    }()
    
    var segueObject: UIStoryboardSegue?
    
    var whatsFeatured: FeaturedViewController = {
        let whatIsFeatured = FeaturedViewController()
        return whatIsFeatured
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        whatsHot.cellSelected = self
        whatsFeatured.cellSelected = self
        setupViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    var detailView = TWLDetailViewController()
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        
    }
    
    func setupViews(){
        self.view.addSubview(settingsBTN)
        self.view.addSubview(searchBTN)
        self.addChildViewController(TWLCollectionViewController())
        let origin:CGPoint = CGPoint(x: 0, y: homeScrollVIew.bounds.origin.y)
        whatsHot.view.bounds = CGRect(origin: origin, size: CGSize(width: deviceScreen.main.bounds.width, height: homeScrollVIew.frame.height + 60))
        homeScrollVIew.addSubview(whatsHot.view)
        self.addChildViewController(FeaturedViewController())
        // caroselView.addSubview(whatsFeatured.view)
        self.view.addSubview(whatsFeatured.view)
        self.view.addSubview(homeScrollVIew)
        
        //TODO: Make a container view for what's hot.
        // caroselView.addSubview(whatsHot.view)
        
        //MARK: PureLayout set up
        
        //Home Posts:
        //whatsHot.view.autoPinEdge(.top, to: .top, of: homeScrollVIew)
        
        homeScrollVIew.contentSize = CGSize(width: deviceScreen.main.bounds.width, height: whatsHot.view.bounds.size.height + whatsHot.cellSize.height)
        
        settingsBTN.autoPinEdge(.top, to: .top, of: self.view, withOffset:40)
        settingsBTN.autoPinEdge(.leading, to: .leading, of: self.view, withOffset:10)
        
        searchBTN.autoPinEdge(.top, to: .top, of: self.view, withOffset:40)
        searchBTN.autoPinEdge(.trailing, to: .trailing, of: self.view, withOffset:-10)
        caroselView.autoSetDimensions(to: CGSize(width: 100, height: 100))
        
        //MARK: Setting creation for what's featured view controller
        whatsFeatured.view.autoSetDimensions(to: CGSize(width: caroselView.frame.size.width, height: caroselView.frame.size.height))
        whatsFeatured.view.autoPinEdge(.top, to: .top, of: self.view)
        whatsFeatured.view.autoPinEdge(.leading, to: .leading, of: self.view)
        
        
        
    }
    
    func cellWasSelected(postContent: [String : String]) {
        
        guard let postTitle = postContent["postTitle"], let postsContent = postContent["postContent"], let postAuthorName = postContent["postAuthorName"], let postAuthorDescription = postContent["descriptionOfPostAuthor"],
            let postAttachmentsUrlString = postContent["postAttachmentsUrlString"] else{ return}
        
        detailView.postTitleString = postTitle
        detailView.postContentString = postsContent
        detailView.postAuthorNameString = postAuthorName
        detailView.aboutAuthorString = postAuthorDescription
        detailView.postAttachmentsUrlString = postAttachmentsUrlString
        
        self.present(detailView, animated: true, completion: nil)
    }
    
}
