//
//  TWLCollectionViewCell.swift
//  appCode
//
//  Created by Havic on 4/18/17.
//  Copyright © 2017 Nerd Gang LLC. All rights reserved.
//

import UIKit

class TWLCollectionViewCell: UICollectionViewCell {
    var padding:CGFloat = 20.0
    var authorName: UILabel = {
        let author = UILabel()
        author.textColor = .white
        author.frame.size.height = 20
        author.frame.size.width = 20
        author.font = UIFont.systemFont(ofSize: 14)
        author.numberOfLines = 4
        return author
    }()
    
    var authorImageView: UIImageView = {
        var authorImage = UIImageView()
        authorImage.translatesAutoresizingMaskIntoConstraints = false
        return authorImage
    }()
    
    var postImageView: UIImageView = {
        var authorImage = UIImageView()
        authorImage.translatesAutoresizingMaskIntoConstraints = false
        return authorImage
    }()
    
    var postTitle: UILabel = {
        var postTitle = UILabel()
        postTitle.textColor = .white
        postTitle.font = UIFont.systemFont(ofSize: 14)
        postTitle.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 0/255, alpha: 0.59)
       postTitle.numberOfLines = 4
        postTitle.preferredMaxLayoutWidth = 100
        postTitle.textAlignment = .center
        return postTitle
    }()
    
    var postSubTitle: UILabel = {
        var subTitle = UILabel()
        subTitle.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 0/255, alpha: 0.59)
        return subTitle
    }()
    
    var postTag: UILabel = {
        let postTagType = UILabel()
        postTagType.backgroundColor = .blue
        postTagType.textColor = .white
        postTagType.font = UIFont(name: "AppleSDGothicNeo-SemiBold", size: 17)
        return postTagType
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        contentView.addSubview(authorName)
        contentView.addSubview(authorImageView)
        contentView.addSubview(postTitle)
        contentView.addSubview(postSubTitle)
        contentView.addSubview(postImageView)
        contentView.addSubview(postTag)
        
//        postImageView.autoPinEdge(.leading, to: .leading, of: contentView,withOffset:10)
//        postImageView.autoPinEdge(.top, to: .top, of: contentView,withOffset:10)
//        postImageView.autoSetDimensions(to: CGSize(width: 50, height: 100))
        
        postTitle.autoPinEdge(.top, to: .top, of: contentView,withOffset:30)
        postTitle.autoPinEdge(.leading, to: .leading, of: postImageView,withOffset:10)
        postTitle.autoSetDimensions(to: CGSize(width: contentView.bounds.size.width - padding, height: 50))
        
        postSubTitle.autoPinEdge(.top, to: .bottom, of: postTitle,withOffset:10)
        postSubTitle.autoPinEdge(.leading, to: .leading, of: postTitle)
        
        authorImageView.autoPinEdge(.leading, to: .trailing, of: postImageView,withOffset:20)
        authorImageView.autoPinEdge(.top, to: .bottom, of: postSubTitle,withOffset:10)
        authorImageView.autoSetDimensions(to: CGSize(width: 30, height: 30))
        
        authorName.autoPinEdge(.leading, to: .trailing, of: authorImageView,withOffset:10)
        authorName.autoPinEdge(.top, to: .top, of: authorImageView,withOffset:5)
//        authorName.autoSetDimensions(to: CGSize(width: contentView.bounds.size.width, height: 50))
        
        postTag.autoPinEdge(.top, to: .top, of: contentView,withOffset:5)
        postTag.autoPinEdge(.leading, to: .leading, of: contentView,withOffset:5)
    }
    
}
