//
//  TWLConstants.swift
//  appCode
//
//  Created by Havic on 2/1/17.
//  Copyright © 2017 Nerd Gang LLC. All rights reserved.
//

import UIKit

class Constants{
    
    var brandColor: UIColor {
        let brandColor = UIColor(red: 56/255, green: 212/255, blue: 198/255, alpha: 1.0)
        return brandColor
    }
    
    var tabBarTitles:TabBarTitles{
        let tabBarTitles = TabBarTitles()
        return tabBarTitles
    }
    
    var collectionViewCell: CollectionVIewStuff {
        let collectionViewStuff = CollectionVIewStuff()
        return collectionViewStuff
        
    }
    
    struct TabBarTitles {
        var tab1: String{
            let tab1 = "What's Hot"
            return tab1
        }
        
        var tab2: String {
            let tab2 = "News"
            return tab2
        }
        
        var tab3: String {
            let tab3 = "Instagram"
            return tab3
        }
        
        var tab4: String {
            let tab4 = "Twitter"
            return tab4
        }
    }
    
    struct CollectionVIewStuff {
        let webServices = WebServices.instance
        var cellCount: Int {
            let cellCount = 5
            return cellCount
        }
        
        var CellSize: CGSize {
            let cellSize = CGSize()
            return cellSize
            
        }
    }
}

enum AuthorImage:String {
    case tejor = "http://egbw50ulcfs0vaux.zippykid.netdna-cdn.com/wp-content/uploads/2011/05/Tej.jpg"
    case ana = "http://techwelike.com/wp-content/uploads/2011/05/Analie-Cruz-Tech-Editor-Blogger-Digital-Lifestyle-Latina-Blogger.png"
    case lina = "http://egbw50ulcfs0vaux.zippykid.netdna-cdn.com/wp-content/uploads/2011/05/xRED_SOULx.jpeg"
    case logo = "http://egbw50ulcfs0vaux.zippykid.netdna-cdn.com/wp-content/uploads/2015/06/twlNoTagline3.png"
}

enum AssestImage:String {
    case setting = "Settings_777777_25"
    case search = "Search_777777_25"
    case fire = "Fire Element_fa000c_32"
    case closeBTN = "icons8-close_window"
}

enum AuthorName:String {
    case tejor = "TJ Jordan"
    case ana = "Analie Cruz"
    case lina = "Angelina Montanez"
}

enum CategoryTitle:String {
    case eSports = "eSports"
    case androidWear = "Android Wear"
    case accessories = "Accessories"
    case featured = "Featured"
    case android = "Android"
    
}
