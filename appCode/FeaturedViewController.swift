//
//  HomeViewController.swift
//  appCode
//
//  Created by Havic on 1/31/17.
//  Copyright © 2017 Nerd Gang LLC. All rights reserved.
//

import UIKit
import PureLayout


class FeaturedViewController: UIViewController,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,JsonDelegate {
    var featuredCollectionView: UICollectionView!
    var cellIdentifier = "Cell"
    var brandConstant = Constants()
    var session = WebServices.instance
    var webServicesObject = WebServices()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 90, height: 200)
        layout.scrollDirection = .horizontal
        let collectionFrame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 200)
        
        featuredCollectionView = UICollectionView(frame: collectionFrame, collectionViewLayout: layout)
        featuredCollectionView.dataSource = self
        featuredCollectionView.delegate = self
        webServicesObject.reloadJson = self
        featuredCollectionView.register(FeaturedViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        featuredCollectionView.backgroundColor = .black
        self.view.addSubview(featuredCollectionView)
        
        guard currentReachabilityStatus != .notReachable else {
            print("We have no net at this time my dude")
            return
        }
        
        webServicesObject.loadThatJson()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return webServicesObject.featuredPostArray.count
    }
    
    
    var twlCollectionView:TWLCollectionViewController = TWLCollectionViewController()
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! FeaturedViewCell
        
        let blogContent = webServicesObject.featuredPostArray[indexPath.row]
        cell.authorName.text = blogContent.postAuthorName
        cell.postTitle.text = blogContent.postTitle
        
        DispatchQueue.global(qos: .background).async {
            let tempImage:UIImage = self.twlCollectionView.getImageFrom(imageString: blogContent.pickedOutAttachments.first!.postAttachmentsUrlString)
            
            DispatchQueue.main.async {
                cell.backgroundView = UIImageView(image: tempImage)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: self.view.frame.height)
    }
    
    func reloadCollectionData() {
        featuredCollectionView.reloadData()
    }
    
    var cellSelected: TWLCollectionViewDelegate?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let blogContent = webServicesObject.featuredPostArray[indexPath.row]
        let blogData:Dictionary = ["postTitle":blogContent.postTitle,"postContent":blogContent.postContent,"postAuthorName":blogContent.postAuthorName,"postAttachmentsUrlString": blogContent.pickedOutAttachments.first!.postAttachmentsUrlString,"descriptionOfPostAuthor": blogContent.postAuthorDescription]
        
        cellSelected?.cellWasSelected(postContent: blogData)
    }
}
