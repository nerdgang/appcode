//
//  AppDelegate.swift
//  appCode
//
//  Created by Havic on 2/20/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//
/***
 *
 *    b.             8 8 8888888888   8 888888888o.   8 888888888o.                    ,o888888o.          .8.          b.             8     ,o888888o.
 *    888o.          8 8 8888         8 8888    `88.  8 8888    `^888.                8888     `88.       .888.         888o.          8    8888     `88.
 *    Y88888o.       8 8 8888         8 8888     `88  8 8888        `88.           ,8 8888       `8.     :88888.        Y88888o.       8 ,8 8888       `8.
 *    .`Y888888o.    8 8 8888         8 8888     ,88  8 8888         `88           88 8888              . `88888.       .`Y888888o.    8 88 8888
 *    8o. `Y888888o. 8 8 888888888888 8 8888.   ,88'  8 8888          88           88 8888             .8. `88888.      8o. `Y888888o. 8 88 8888
 *    8`Y8o. `Y88888o8 8 8888         8 888888888P'   8 8888          88           88 8888            .8`8. `88888.     8`Y8o. `Y88888o8 88 8888
 *    8   `Y8o. `Y8888 8 8888         8 8888`8b       8 8888         ,88           88 8888   8888888 .8' `8. `88888.    8   `Y8o. `Y8888 88 8888   8888888
 *    8      `Y8o. `Y8 8 8888         8 8888 `8b.     8 8888        ,88'           `8 8888       .8'.8'   `8. `88888.   8      `Y8o. `Y8 `8 8888       .8'
 *    8         `Y8o.` 8 8888         8 8888   `8b.   8 8888    ,o88P'                8888     ,88'.888888888. `88888.  8         `Y8o.`    8888     ,88'
 *    8            `Yo 8 888888888888 8 8888     `88. 8 888888888P'                    `8888888P' .8'       `8. `88888. 8            `Yo     `8888888P'
 */
/*
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
    let rootViewController = ViewController()
    var remoteConfig:RemoteConfig?


  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.backgroundColor = .white
    window?.rootViewController = rootViewController
    window?.makeKeyAndVisible()
    FirebaseApp.configure()
    self.remoteConfig = RemoteConfig.remoteConfig()
    let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true)
    remoteConfig?.configSettings = remoteConfigSettings!
    return true
  }

  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }


}
