//
//  PostsCollectionVC.swift
//  appCode
//
//  Created by Havic on 4/24/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit
import NerdKit

class PostsCollectionVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,JsonDelegate {
    
    let reuseIdentifier = "cell"
    var webInstance = WebServices()
    @IBOutlet weak var myCollection: UICollectionView!
    var blogUrl: URL?
    var blogData = Data()
    var appConfig = AppConfig.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webInstance.reloadJson = self
        guard currentReachabilityStatus != .notReachable else {
            print("NO connection, you better find one.")
            return
        }
        webInstance.loadThatJson()
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        self.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        // #warning Incomplete implementation, return the number of sections
        return webInstance.blogArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // #warning Incomplete implementation, return the number of items
        return webInstance.blogArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeCollectionViewCell
        let blogObjects = webInstance.blogArray[indexPath.row]
        
        // Configure the cell
        cell.authorName.text = blogObjects.authorName + " on " + blogObjects.postDate
        cell.authorName.textColor = UIColor.black
        cell.postShortDescription.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        cell.postShortDescription.textColor = UIColor.white
        cell.postShortDescription.text = blogObjects.postTitle
        let logo = appConfig.plistPath!["logo"] as! String
        cell.postThumbnail.image = UIImage(named: logo)
        
        // Multithreaded Method for downloading of the images using GCD(Grand Central Dispatch)
        let backgroundQueue = DispatchQueue(label: "com.app.queue",qos: .background,target: nil)
        
        backgroundQueue.async {
            // Background thread
            let imageIssue = blogObjects.thumbnailUrl
            if (imageIssue != nil){
                self.blogUrl = URL(string: blogObjects.thumbnailUrl!.addingPercentEscapes(using: String.Encoding.utf8)!)!
                self.blogData = try! Data(contentsOf: self.blogUrl!)
                let currentImage: UIImage = UIImage(data: self.blogData)!
            DispatchQueue.main.async(execute: {
                // UI Updates back on the Main thread:
                cell.postThumbnail.image = currentImage
            })
        }
            
        }
        
        return cell
    }
    
    func reloadCollectionData() {
        myCollection.reloadData()
    }
    
    // MARK: UICollectionViewDelegate
    
    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return false
     }
     
     override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
     return false
     }
     
     override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
     
     }
     */
    
}
