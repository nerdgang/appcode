//
//  DataLayer.swift
//  appCode
//
//  Created by Havic on 2/4/17.
//  Copyright © 2017 Nerd Gang LLC. All rights reserved.
//

import UIKit

struct PostCategories {
    var postCategoriesTitle: String
    var postCategoriesDescription: String
    
    init(catDict: NSDictionary) {
        self.postCategoriesTitle = catDict["title"] as! String
        self.postCategoriesDescription = catDict["description"] as! String
    }
}

struct PostTags {
    var postTagsTitle:String
    
    init(tagsDict:NSDictionary) {
        self.postTagsTitle = tagsDict["title"] as! String
    }
}

struct PostAttachments {
    var postAttachmentsUrlString:String
    var postAttachmentsTitle:String
    
    init(attachements:NSDictionary) {
        self.postAttachmentsUrlString = attachements["url"] as! String
        self.postAttachmentsTitle = attachements["title"] as! String
    }
}

struct PostComments {
    var postComments:String
    var postComment_count:Int
    
    init(postsComments:NSDictionary) {
        self.postComments = ""
        self.postComment_count = postsComments["comment_count"] as! Int
    }
}

struct PostStructure {
    var postTitle: String
    var postContent: String
    var postExcerpt: String
    var postDate: String
    var postAuthorName:String
    //var postThumbnailUrl: String
    var postAuthorNickname:String
    var postAuthorDescription:String
    var pickedOutCat: [PostCategories]
    var pickedOutComments: [PostComments]
    var pickedOutAttachments: [PostAttachments]
    var pickedOutTags: [PostTags]
    
    init(dataDictionary:NSDictionary) {
        self.postTitle = dataDictionary["title"] as! String
        self.postTitle = self.postTitle.changeHtmlToString()
        self.postContent = dataDictionary["content"] as! String
        self.postExcerpt = dataDictionary["excerpt"] as! String
        self.postDate = dataDictionary["date"] as! String
        self.postDate = self.postDate.formattedDate()
        //self.postThumbnailUrl = dataDictionary["thumbnail"] as! String
        
        //Author:
        let author = dataDictionary["author"] as! NSDictionary
        self.postAuthorName = author["name"] as! String
        self.postAuthorNickname = author["nickname"] as! String
        self.postAuthorDescription = author["description"] as! String
        
        //Categories:
        pickedOutCat = [PostCategories]()
        let categories = dataDictionary["categories"] as! [NSDictionary]
        
        for cats in categories{
            let catsTitle = PostCategories(catDict: cats)
            pickedOutCat.append(catsTitle)
        }
        
        //Comments:
        pickedOutComments = [PostComments]()
        _ = dataDictionary["comment_count"]
        
        
        //Attachments:
        pickedOutAttachments = [PostAttachments]()
        let attachments = dataDictionary["attachments"] as! [NSDictionary]
        
        for eachAttachment in attachments{
            let eachAttachment = PostAttachments(attachements: eachAttachment)
            pickedOutAttachments.append(eachAttachment)
        }
        
        //Tags:
        pickedOutTags = [PostTags]()
        let tags = dataDictionary["tags"] as! [NSDictionary]
        
        for eachTag in tags{
            let eachPostTag = PostTags(tagsDict: eachTag)
            pickedOutTags.append(eachPostTag)
        }
    }
}
