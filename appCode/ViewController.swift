//
//  ViewController.swift
//  appCode
//
//  Created by Havic on 2/20/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit
import PureLayout

class ViewController: UITabBarController, UITabBarControllerDelegate {
    var brandStyling = Constants()
  
    override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
        
        //Assign self for delegate for that ViewController can respond to UITabBarControllerDelegate methods
        self.delegate = self
        self.tabBar.tintColor = brandStyling.brandColor
        
        // Create Tab one
        let tabOne = HomeViewController()
            //FeaturedViewController()
        let tabOneBarItem = UITabBarItem(title: brandStyling.tabBarTitles.tab1, image: UIImage(named: AssestImage.fire.rawValue)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: AssestImage.fire.rawValue))
        
        tabOne.view.backgroundColor = .clear
        tabOne.tabBarItem = tabOneBarItem
        
        
        // Create Tab two
        let tabTwo = PostsCollectionVC()
        let tabTwoBarItem2 = UITabBarItem(title: brandStyling.tabBarTitles.tab2, image: UIImage(named: "defaultImage2.png"), selectedImage: UIImage(named: "selectedImage2.png"))
        
        tabTwo.tabBarItem = tabTwoBarItem2
        
        //Create Tab three
        let tabThree = InstagramViewController()
        let tabThreeBarItem3 = UITabBarItem(title: brandStyling.tabBarTitles.tab3, image: UIImage(named: "defaultImage2.png"), selectedImage: UIImage(named: "defaultImage2.png"))
        tabThree.tabBarItem = tabThreeBarItem3
        
        //Create Tab four
        let tabFour = TwitterViewController()
        let tabFourBarItem4 = UITabBarItem(title: brandStyling.tabBarTitles.tab4, image: UIImage(named: "defaultImage2.png"), selectedImage: UIImage(named: "defaultImage2.png"))
        tabFour.tabBarItem = tabFourBarItem4
        
        self.viewControllers = [tabOne, tabTwo, tabThree, tabFour]
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
 
    }
    
    // UITabBarControllerDelegate method
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
    }
  
}
