//
//  TWL Extensions.swift
//  appCode
//
//  Created by Havic on 5/29/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

extension UIImage{
    
    convenience init?(createFromThis:String?) {
        guard let urlString = createFromThis, let stringUrl = URL(string: urlString), let dataObject = try? Data(contentsOf: stringUrl) else {
            self.init()
            return
        }
        
        self.init(data: dataObject)
    }
    
    convenience init(imageFromString:String) {
        var dataObject:Data?
        do{
            
        try dataObject = Data(contentsOf: URL(string: imageFromString)!)
        }
        catch{
            dataObject = nil
        }
        guard let myDataObject = dataObject else {
            self.init()
            return
        }
        self.init(data: myDataObject)!
    }
}

extension String {
    
    /**
     
     Strips extra HTML characters from String and replaces with regular characters.
     
     - returns: String without Occurrence of special characters.
     
     */
    
    mutating func changeHtmlToString() -> String{
        var newString = self
        newString = newString.replacingOccurrences(of: "&#038;", with: "&")
        newString = newString.replacingOccurrences(of: "&#8211;", with: "-")
        newString = newString.replacingOccurrences(of: "&#8217;", with: "'")
        newString = newString.replacingOccurrences(of: "&#8220;", with: "\" ")
        newString = newString.replacingOccurrences(of: "&#8221;", with: "\" ")
        
        return newString
    }
    
    /**
     
     Format the way the date looks.
     
     - returns: String output of a formated date.
     
     */
    
    func formattedDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let tempDate = dateFormatter.date(from: self as String)
        dateFormatter.dateFormat = "EE MMM dd, yyyy"
        return dateFormatter.string(from: tempDate!)
    }
}

extension TWLCollectionViewController{
    
  func setupViews(homePostView: inout UICollectionView!){
        
        _ = homePostView
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 90, height: 120)
        homePostView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        homePostView?.dataSource = self as UICollectionViewDataSource
        homePostView?.delegate = self as UICollectionViewDelegate
        homePostView?.register(TWLCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        homePostView?.backgroundColor = .lightGray
        self.view.addSubview(homePostView)
    }
    
    func getImageFrom(imageString:String)->UIImage{
        let imageUrl = URL(string: imageString)
        
        let imageData: Data?
        
        do{
            imageData = try Data(contentsOf: imageUrl!)
        }catch{
            imageData = nil
        }
        
        guard let authorImage = UIImage(data: imageData!) else{ return UIImage(named: "logo")!}
        
        return authorImage
    }
    
    /**
     
     Initializes and returns an UIImage object.
     
     - parameter imageString: The AuthorImage enum which is a formated String
     
     - returns: An initialized UIImage object, or default logo if the method could not initialize the image from the specified data
     */
    
    func grabUserImage(imageString:AuthorImage)->UIImage{
        let imageUrl = URL(string: imageString.rawValue)
        
        let imageData: Data?
        
        do{
            imageData = try Data(contentsOf: imageUrl!)
        }catch{
            imageData = nil
        }
        
        guard let authorImage = UIImage(data: imageData!) else{ return UIImage(named: "logo")!}
        
        return authorImage
    }
    
    /**
     
     Initializes and returns an UIImage object.
     
     - parameter authorImageString: The AuthorImage enum which is a formated String
     - parameter cellForAuthorImage: The CollectionViewCell that the image object will be placed in.
     
     - returns: An initialized UIImage object, or nil if the method could not initialize the image from the specified data
     */
    
    func bgDownloadTask(authorImageString:AuthorImage,cellForAuthorImage:TWLCollectionViewCell){
        DispatchQueue.global(qos: .background).async {
            let tempImage:UIImage = self.grabUserImage(imageString: authorImageString)
            DispatchQueue.main.async {cellForAuthorImage.authorImageView.image = tempImage}}
    }

}
