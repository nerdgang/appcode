//
//  TWLCollectionViewController.swift
//  appCode
//
//  Created by Havic on 4/18/17.
//  Copyright © 2017 Nerd Gang LLC. All rights reserved.
//

import UIKit

protocol TWLCollectionViewDelegate {
    func cellWasSelected(postContent:[String:String])
}

class TWLCollectionViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,JsonDelegate {
    let webservices = WebServices()
    
    var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        
        return refresh
    }()
    
    var homePostView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        webservices.reloadJson = self
        webservices.loadThatJson()
        setupViews(homePostView: &homePostView)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    func reloadCollectionData() {
        refreshControl.endRefreshing()
        homePostView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return webservices.postArray.count
        
    }
    
    var cellReuseIdentifier:String = "cell"
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! TWLCollectionViewCell
        
        let blogContent = webservices.postArray[indexPath.row]
        
        let datePosted = blogContent.postDate
        let byLine:String = "\(datePosted)"
        let authorNameWithByLine:String = byLine
        
        cell.authorName.text = authorNameWithByLine
        cell.authorName.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 0/255, alpha: 0.59)
        cell.backgroundColor = .darkGray
        cell.postTitle.text = blogContent.postTitle
        cell.postTag.text = blogContent.pickedOutCat.first?.postCategoriesTitle
        
        DispatchQueue.global(qos: .background).async {
            let tempImage:UIImage = self.getImageFrom(imageString: blogContent.pickedOutAttachments.first!.postAttachmentsUrlString)
            
            DispatchQueue.main.async {
                cell.backgroundView = UIImageView(image: tempImage)
            }
        }
        
        /*
         //code to show author picture next to post...takes in a AuthorName ->UIImage
         //May add this code in again if Tej says it's a go.
         
         //TODO: Delete this code if it's not approved for production.
         
         switch blogContent.postAuthorName {
         case AuthorName.ana.rawValue:
         bgDownloadTask(authorImageString: .ana, cellForAuthorImage: cell)
         break
         case AuthorName.tejor.rawValue:
         bgDownloadTask(authorImageString: .tejor, cellForAuthorImage: cell)
         break
         case AuthorName.lina.rawValue:
         bgDownloadTask(authorImageString: .lina, cellForAuthorImage: cell)
         break
         default:
         bgDownloadTask(authorImageString: .logo, cellForAuthorImage: cell)
         }
         */
        
        switch blogContent.pickedOutCat.first!.postCategoriesTitle {
        case String(describing: CategoryTitle.eSports):
            cell.postTag.backgroundColor = .purple
            break
        case String(describing: CategoryTitle.androidWear):
            cell.postTag.backgroundColor = .gray
            break
        case String(describing: CategoryTitle.featured):
            cell.postTag.backgroundColor = .red
            break
        case String(describing: CategoryTitle.android):
            cell.postTag.backgroundColor = .magenta
        case String(describing: CategoryTitle.accessories):
            cell.postTag.backgroundColor = .green
            break
        default:
            cell.postTag.backgroundColor = .blue
        }
        
        return cell
        
    }
    
    let cellSize:CGSize = CGSize(width: UIScreen.main.bounds.width, height: 150)
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return cellSize
    }
    
    var cellSelected: TWLCollectionViewDelegate?
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let blogContent = webservices.postArray[indexPath.row]
        let blogData:Dictionary = ["postTitle":blogContent.postTitle,"postContent":blogContent.postContent,"postAuthorName":blogContent.postAuthorName,"postAttachmentsUrlString": blogContent.pickedOutAttachments.first!.postAttachmentsUrlString,"descriptionOfPostAuthor": blogContent.postAuthorDescription]
        
        cellSelected?.cellWasSelected(postContent: blogData)
    }
    
    
    //TODO: Make a auto refresh function which will call handleRefresh on intertervals as long as user is still active on this view.
    
    //MARK: Pull to refresh
    func handleRefresh(refreshControl:UIRefreshControl){
        webservices.loadThatJson()
    }
    
}
