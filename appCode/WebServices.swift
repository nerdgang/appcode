//
//  WebServices.swift
//  appCode
//
//  Created by Havic on 2/21/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit
import NerdKit

protocol JsonDelegate{
    func reloadCollectionData()
}

class WebServices: NSObject {
    
    var blogArray = [Post]()
    var reloadJson: JsonDelegate?
    static var appConfig = AppConfig.instance
    
    var blogUrl: URL = {
        let jsonUrl = URL(string: "http://techwelike.com/api/get_posts/?count=25/")!
        return jsonUrl
    }()
    var controls = NKControls()
    static let instance = WebServices()
    var postArray = [PostStructure]()
    var featuredPostArray = [PostStructure]()
    let backgroundQueue = DispatchQueue(label: "com.app.queue",qos: .background,target: nil)
    
    func loadThatJson(){
        
        guard currentReachabilityStatus != .notReachable else {
            print("No web homie")
            return
        }
        
        let dataTask: URLSessionDataTask = URLSession.shared.dataTask(with: blogUrl) { (data, response, error) -> Void in
            let jsonDic: NSDictionary?
                
            do{
                jsonDic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary
            }
            catch _ {
                jsonDic = [:]
            }
            
            let tempArray = jsonDic?["posts"] as! [NSDictionary]
            
            for objectsData in tempArray {
                let featuredCategory = objectsData["categories"] as! [NSDictionary]
                for features in featuredCategory where features["id"] as! Int == 3191{
                let posts = PostStructure(dataDictionary: objectsData)
                    self.featuredPostArray.append(posts)
                }
                let postData = PostStructure(dataDictionary: objectsData)
                self.postArray.append(postData)
                
            }
            
            // Call main thread for UI changes:
            DispatchQueue.main.async {
                self.reloadJson?.reloadCollectionData()
            }
        }
        dataTask.resume()
    }
    
}
