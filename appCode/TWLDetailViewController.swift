//
//  TWLDetailViewController.swift
//  appCode
//
//  Created by Havic on 5/23/17.
//  Copyright © 2017 Nerd Gang LLC. All rights reserved.
//

import UIKit

class TWLDetailViewController: UIViewController {
    typealias deviceScreen = UIScreen
    
    var postFeaturedImage: UIImageView = {
        var postImage = UIImageView()
        postImage.translatesAutoresizingMaskIntoConstraints = false
        postImage.backgroundColor = .green
        return postImage
    }()
    
    var postTitle: UILabel = {
        var title = UILabel()
        title.text = "Text for title currently"
        return title
    }()
    
    var postContent: UILabel = {
        var content = UILabel()
        content.text = "This is going to be the content from the post, so of course there's going to be a lot of stuff that needs to go in here because well it's a post and all."
        content.numberOfLines = 0
        return content
    }()
    
    var authorName: UILabel =  {
        var name = UILabel()
        name.text = "Name of the author here"
        return name
    }()
    
    var aboutAuthor: UILabel = {
        var authorDetail = UILabel()
        authorDetail.text = "Information about the author goes here"
        return authorDetail
    }()
    
    var contentScrollView: UIScrollView = {
        let homeScroll = UIScrollView()
        homeScroll.frame = CGRect(x: 0, y: 10, width: deviceScreen.main.bounds.width, height: deviceScreen.main.bounds.height)
        homeScroll.contentSize = CGSize(width: deviceScreen.main.bounds.width, height: 2000)
        homeScroll.backgroundColor = .gray
        homeScroll.isScrollEnabled = true
        return homeScroll
    }()
    
    var postTitleString = ""
    var postContentString = ""
    var postAuthorNameString = ""
    var postAttachmentsUrlString = ""
    var aboutAuthorString = ""
    
    var closeBTN: UIButton = {
        var close = UIButton(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        return close
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        layoutTheseViews()
        postTitle.text = postTitleString
        postContent.text = postContentString
        authorName.text = postAuthorNameString
        aboutAuthor.text = aboutAuthorString
        postFeaturedImage.image = UIImage(imageFromString: postAttachmentsUrlString)
        closeBTN.setImage(UIImage(named: String(describing: AssestImage.closeBTN)), for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func layoutTheseViews(){
        //self.view.addSubview(postFeaturedImage)
        contentScrollView.addSubview(postTitle)
        contentScrollView.addSubview(postContent)
        contentScrollView.addSubview(authorName)
        contentScrollView.addSubview(aboutAuthor)
        contentScrollView.addSubview(postFeaturedImage)
        self.view.addSubview(contentScrollView)
        self.view.addSubview(closeBTN)
        
        closeBTN.addTarget(self, action: #selector(close), for: .allTouchEvents)
        
        /* contentScrollView.addSubview(postFeaturedImage)
         contentScrollView.addSubview(postTitle)
         contentScrollView.addSubview(postContent)
         contentScrollView.addSubview(authorName)
         contentScrollView.addSubview(aboutAuthor)
         */
        // self.view.addSubview(contentScrollView)
        postFeaturedImage.image = UIImage(named: "AppIcon")
        postFeaturedImage.autoSetDimensions(to: CGSize(width: UIScreen.main.bounds.size.width, height: 150))
        postFeaturedImage.autoPinEdge(.top, to: .top, of: self.view,withOffset:20)
        postFeaturedImage.autoPinEdge(.leading, to: .leading, of: self.view)
        self.view.backgroundColor = .gray
        
        postTitle.autoPinEdge(.top, to: .bottom, of: postFeaturedImage,withOffset:10)
        postTitle.autoCenterInSuperview()
        postTitle.autoSetDimensions(to: CGSize(width: 300, height: 50))
        
        postContent.autoPinEdge(.top, to: .bottom, of: postTitle)
        postContent.autoPinEdge(.leading, to: .leading, of: self.view,withOffset:15)
        postContent.autoSetDimensions(to: CGSize(width: 350, height: 200))
        
        authorName.autoPinEdge(.top, to: .bottom, of: postContent)
        authorName.autoPinEdge(.leading, to: .leading, of: postTitle)
        
        aboutAuthor.autoPinEdge(.top, to: .bottom, of: authorName,withOffset:10)
        aboutAuthor.autoPinEdge(.leading, to: .leading, of: authorName)
        
    }
    
    func close() {
        
        self.dismiss(animated: true, completion: nil)
    }
}
