//
//  FeaturedViewCell.swift
//  appCode
//
//  Created by Havic on 2/1/17.
//  Copyright © 2017 Nerd Gang LLC. All rights reserved.
//

import UIKit
import PureLayout

class FeaturedViewCell: UICollectionViewCell {
    let padding = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    
    lazy var authorName: UILabel = {
        let authorName = UILabel(forAutoLayout: ())
        authorName.text = "Tej"
        authorName.textColor = .white
        return authorName
    }()
    
    lazy var postTitle: UILabel = {
        let postTitle = UILabel(forAutoLayout: ())
        postTitle.textColor = .white
        postTitle.numberOfLines = 3
        postTitle.text = "Title of post will be here"
        return postTitle
    }()
    
    let postTitleBGView: UIView = {
        let postTitleBGView = UIView()
        postTitleBGView.backgroundColor = .black
        return postTitleBGView
    }()
    
    let postBgView: UIView = {
        let postBgView = UIView()
        return postBgView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews(){
        backgroundColor = .blue
        contentView.addSubview(authorName)
        postTitleBGView.addSubview(postTitle)
        contentView.addSubview(postTitleBGView)
        
        //PureLayout setup:
        authorName.autoPinEdge(.top, to: .top, of: contentView)
        authorName.autoPinEdge(.left, to: .left, of: contentView)
        
//        postTitle.autoPinEdge(.top, to: .top, of: postTitleBGView)
//        postTitle.autoPinEdge(.left, to: .right, of: contentView)
        postTitle.autoCenterInSuperview()
        postTitle.autoPinEdgesToSuperviewEdges(with: padding)
        
        postTitleBGView.autoPinEdge(.top, to: .bottom, of: authorName, withOffset: 40)
        postTitleBGView.autoSetDimensions(to: CGSize(width: self.frame.size.width, height: 30))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
