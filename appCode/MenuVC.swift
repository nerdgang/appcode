//
//  MenuVC.swift
//  appCode
//
//  Created by Havic on 4/24/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

class MenuVC: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var myTable: UITableView!
    var menuItems: [String] = ["Posts", "Galleries", "Sections", "Favorites", "Social"]
    let reuseIdentifier = "cell"
    var webInstance = WebServices()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        return menuItems.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        // Configure the cell...
        let menuTitles = menuItems[indexPath.row]
        
        cell.textLabel?.text = menuTitles
        cell.textLabel?.textAlignment = .center
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch (indexPath as NSIndexPath).row{
        case 0:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostsVC") as! PostsCollectionVC
            self.present(vc, animated: true, completion: nil)
            break
        default:
            break
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
