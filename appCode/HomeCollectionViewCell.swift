//
//  HomeCollectionViewCell.swift
//  appCode
//
//  Created by Havic on 4/24/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit
import PureLayout


class HomeCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var a1uthorProfileImage: UIImageView!
    @IBOutlet weak var a1uthorName: UILabel!
    @IBOutlet weak var p1ostShortDescription: UILabel!
    @IBOutlet weak var p1ostThumbnail: UIImageView!
    
    lazy var authorProfileImage: UIImageView = {
        let authorProfileImage = UIImageView()
        return authorProfileImage
    }()
    
    lazy var authorName: UILabel = {
        let authorName = UILabel()
        return authorName
    }()
    
    lazy var postShortDescription: UILabel = {
        let postShortDescription = UILabel()
        return postShortDescription
    }()
    
    lazy var postThumbnail: UIImageView = {
        let postThumbnail = UIImageView()
        return postThumbnail
    }()
    
    override init(frame:CGRect){
        super.init(frame: frame)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews(){
        contentView.addSubview(authorProfileImage)
        contentView.addSubview(authorName)
        contentView.addSubview(postShortDescription)
        contentView.addSubview(postThumbnail)
        
        //PureLayout Setup
    }
}
