//
//  BlogObjects.swift
//  appCode
//
//  Created by Havic on 2/21/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

class BlogObjects {
    
    var authorName: String
    var postTitle:  String
    var postDate: String
    var thumbnailUrl: String?
    var postImage: UIImage{return UIImage(imageFromString: thumbnailUrl!)}
    var catArr = NSMutableArray()
    
    init(jsonDic:NSDictionary) {
        let tempDic = jsonDic["author"] as! NSDictionary
        self.authorName = tempDic["name"] as! String
        self.postTitle = jsonDic["title"] as! String
        self.postTitle = self.postTitle.changeHtmlToString()
        self.postDate = jsonDic["date"] as! String
        self.postDate = self.postDate.formattedDate()
        self.thumbnailUrl = jsonDic["thumbnail"] as? String
        let tempArr = jsonDic["categories"] as? NSArray
        
        for dicts in tempArr as! [NSDictionary] {
            let tempString = dicts["title"] as? String
            catArr.add(tempString!)
        }
    }
}

struct Post {
    var authorName: String
    var postTitle:  String
    var postDate: String
    var thumbnailUrl: String?
    var categoriesOfBlog: String?
    var catArr = NSMutableArray()
    
    init(jsonDic:NSDictionary) {
        let tempDic = jsonDic["author"] as! NSDictionary
        authorName = tempDic["name"] as! String
        postTitle = jsonDic["title"] as! String
        postTitle = self.postTitle.changeHtmlToString()
        postDate = jsonDic["date"] as! String
        postDate = self.postDate.formattedDate()
        thumbnailUrl = jsonDic["thumbnail"] as? String
        let tempArr = jsonDic["categories"] as? NSArray
        for dicts in tempArr as! [NSDictionary] {
            let tempString = dicts["title"] as? String
            catArr.add(tempString!)
            
        }
    }
}
